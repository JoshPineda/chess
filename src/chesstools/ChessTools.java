package chesstools;

import board.Space;
import piece.Bishop;
import piece.King;
import piece.Knight;
import piece.Pawn;
import piece.Piece;
import piece.Queen;
import piece.Rook;
/**
 * <p>Helper functions used to check input and promotions</p>
 * @author Joshua Pineda
 * 
 */
public class ChessTools {
	/**
	 * <p>Checks if a given character is within a through h</p>
	 * @param c
	 * @return whether or not c is within the chessboard's letter row range
	 */
	static boolean checkletter(char c) {
		String pletters = "abcdefgh";
		if (pletters.indexOf(c) >= 0)
			return true;
			
		return false;
	}
	/**
	 * <p>Checks if a digit is within 1-8</p>

	 * @param c
	 * @return if c(row value of user input) is within range 1-8 (inclusive)
	 */
	static boolean checkdigit(char c) {
		int temp = Character.getNumericValue(c);
		if (temp < 9 && temp >= 0)
			return true;
		return false;
	}
	/**
	 * <p>Helper function to print error messages (mainly for debugging in development)</p>
	 * @param err
	 */
	public static void printerror(String err) {
		System.out.println(err);
	}
	/**
	 * <p>Primarily used to check the validity of a "promotion move" (ex. a7 a8 Q)</p>
	 * @param src - the Space of the piece being moved
	 * @param aux - What kind of piece the user is attempting to promote to
	 * @return Validity of the user's input promotion move.
	 */
	public static boolean isValidPromotion(Space src, Space aux) {
		/**
		 * Check if pawn
		 * */
		if (src.getPiece().getType() != 'p')
			return false;
		/**
		 * Promos only valid if they specify valid type (1 Letter)
		 * */
		if (aux.getAux().length() > 1)
			return false;
		String promos = "NBQR";
		if (!(promos.contains(aux.getAux())) )
			return false;
		
		return true;
	}
	/**
	 * <p>Checks if the user input passed into terminal is in proper format to move pieces on the board</p>
	 * @param input - string that user passes into terminal
	 * @return validity of the input passed to terminal
	 */
	public static boolean checkMove(String input) {
		/*If they pass nothing*/
		if (input == null) {
			printerror("Illegal move, try again");
			return false;
		}
		/**
		 * If the they are trying to resign
		 * */
		String[] moves = input.split(" ");
		if (moves.length == 1) {
			if (moves[0].equals("resign"))
				return true;
			else {
				printerror("Illegal move, try again");
				return false;
			}
		}
		String m1= moves[0], m2 = moves[1];
		
		
		/**
		 * If incorrect length of input
		 */
		if (m1.length() != 2 || m2.length() != 2){
			printerror("Illegal move, try again");
			return false;
		}
		/**
		 * Check if letter is in range at all
		 */
		if (!checkletter(m1.charAt(0)) || !checkletter(m2.charAt(0))){
			printerror("Illegal move, try again");
			return false;
		}
		/**
		 * Check if second char in each move is a number
		 */
		if (!Character.isDigit(m1.charAt(1)) || !Character.isDigit(m2.charAt(1))){
			printerror("Illegal move, try again");
			return false;
		}
		/**
		 * Check if digit interpretation is within proper range.
		 */
		if (!checkdigit(m1.charAt(1)) || !checkdigit(m2.charAt(1))){
			printerror("Illegal move, try again");
			return false;
		}
		/**
		 * Check if move is to itself
		 */
		if (m1.equals(m2)) {
			printerror("Illegal move, try again");
			return false;
		}
			
			

		return true;
	}
	/**
	 * <p> Method convert(String input) turns the user's string input into positions on the chess board </p>
	 * @author Joshua Pineda
	 * @param input - The string the user enters into the console
	 * @return an array of 'Space' Objects representing their move on the board.
	 */
	public static Space[] convert (String input) {
		Space[] mv = new Space[3];
		String[] moves = input.split(" ");
		String m1,m2;
		if (moves.length == 3) {
			m1 = moves[0];
			m2 = moves[1];
			/**
			 * Get the auxiliary string for draw or promotion
			 * */
			mv[2] = new Space();
			mv[2].setAux(moves[2]);
		}
		else {
			m1 = moves[0];
			m2 = moves[1];
		}
		
		
		
		
		String pletters = "abcdefgh";
		/**
		 * set col to 1st character in first part of input
		 */
		int m1col = pletters.indexOf(m1.charAt(0));
		/**
		 * set row to 2nd character in first part of input
		 */
		int m1row = 8 - Character.getNumericValue(m1.charAt(1));
		
		/**
		 * repeat for second part of their input
		 */
		int m2col = pletters.indexOf(m2.charAt(0));
		int m2row = 8 - Character.getNumericValue(m2.charAt(1));
		
		mv[0] = new Space(m1row,m1col);
		mv[1] = new Space(m2row,m2col);
		
		
		return mv;
	}
	
	


}
